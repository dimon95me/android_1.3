package examplesoureit.registerform;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    //    @BindView(R.id.action)
//    Button action;
    @BindView(R.id.login)
    EditText login;
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.phone_number)
    EditText phoneNumber;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.password_retry)
    EditText passwordRetry;
    @BindView(R.id.verify_result)
    TextView resultText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.action)
    void onClick() {
        StringBuffer stringBuffer = new StringBuffer();
        String tempolar;

        tempolar = login.getText().toString();
        if (tempolar.equalsIgnoreCase("")) stringBuffer.append("Field login is empty\n");

        tempolar = email.getText().toString();
        if (tempolar.equalsIgnoreCase("")) stringBuffer.append("Field email is empty\n");
        if (!tempolar.contains("@") || !tempolar.contains("."))
            stringBuffer.append("You are miss @ or '.'\n");//working
        if (tempolar.indexOf(".", tempolar.indexOf("@")) < 0)
            stringBuffer.append("You must write '.' after '@'\n");//working

        tempolar = phoneNumber.getText().toString();
        if (tempolar.equalsIgnoreCase("")) stringBuffer.append("Field phone is empty\n");
        if (tempolar.indexOf("+", 0) > 0 || !tempolar.startsWith("+"))
            stringBuffer.append("enter '+' in phone first\n");
        if (tempolar.length() < 10 || tempolar.length() > 15)
            stringBuffer.append("Phone number size is not corrent\n");
        if (!containsArray(tempolar)) stringBuffer.append("Field hawe wrong symbols\n");

        if (password.getText().toString().equalsIgnoreCase(""))
            stringBuffer.append("Field password is empty\n");
        if (password.getText().length()<6) stringBuffer.append("Your password is too short\n");
        if (passwordRetry.getText().toString().equalsIgnoreCase(""))
            stringBuffer.append("Field second password is empty\n");
        if (!password.getText().toString().equals(passwordRetry.getText().toString()))
            stringBuffer.append("password fields are nuot equals");


        resultText.setText(stringBuffer.toString());
        System.out.println(stringBuffer.toString());
    }

    public boolean containsArray(String string) {
        char[] numbersArray = {'1', '2', '3', '4', '5', '6', '7', '8', '9'};

        for (int j = 1; j < string.length(); j++) {
            boolean isContain = false;
            for (char i : numbersArray) {
                if (string.charAt(j)== i) {
                    isContain = true;
                    System.out.println(isContain);
                    continue;
                }
            }
            if (!isContain) return false;
//            System.out.println(isContain);
        }
        return true;
    }
}
